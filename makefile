# Copy the WeGood stub and cdk module binaries to this directory.

export MVN_REPO := ~/.m2/repository
export WEGOOD_CDK_VERSION := 1.0
export WEGOOD_STUB_VERSION := 1.0

.PHONY: update

# Copy the binaries and then do a git commit and push.
update:
	cp $(MVN_REPO)/com/cliffberg/devopsforagilecoaches/wegood.cdk/$(WEGOOD_CDK_VERSION)/wegood.cdk-$(WEGOOD_CDK_VERSION).jar .
	cp $(MVN_REPO)/com/cliffberg/devopsforagilecoaches/wegood.stub/$(WEGOOD_STUB_VERSION)/wegood.stub-$(WEGOOD_STUB_VERSION).jar .
	git add .
	git commit -am "Updated binaries"
	git push
